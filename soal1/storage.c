#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
    Alur program :
    1. Download data dari User Kaggle.com (.zip)
    2. Extract code 
    3. Analisis data (FIFA23_official_data.csv)
*/


void getDatasets() {
    int getStatus = system("kaggle datasets download -d bryanb/fifa-player-stats-database -p ./");

    if (getStatus != 0) {
        printf("Oops! Terjadi kesalahan, gagal mengunduh datasets.\n");

        exit(1);
    }
}

void extrDatasets() {
    int extrStatus = system("unzip ./fifa-player-stats-database.zip");

    if (extrStatus != 0) {
        printf("Oops! Terjadi kesalahan, gagal ekstrak datasets.\n");

        exit(1); 
    }
}

void dataAnalysis() {
    FILE *csvData = fopen("./FIFA23_official_data.csv", "r");
    
    if (csvData == NULL) {
        printf("Gagal membuka file CSV\n");
        return;
    }


    printf("Nama\t\t|   Umur   |   \t\t\tURL Foto\t\t\t   |    Kewarganegaraan\t |    Potensi\t    |   \tKlub\t\t\n");
    printf("----------------------------------------------------------------------------------------\n");

    // data baris csv
    char dataLine[2000];

    // remove header
    fgets(dataLine, sizeof(dataLine), csvData);

    // looping data csv
    while (fgets(dataLine, sizeof(dataLine), csvData)) {
        
        char *current_line = strdup(dataLine);

        // get token (pemain)
        char *name = strtok(current_line, ",");
        name = strtok(NULL, ",");

        if (name == NULL) {
            free(current_line);
            continue;
        }

        // var hasil nama
        char *playerName = strdup(name);

        // Membaca info pribadi pemain
        char *ageVal = strtok(NULL, ",");
        char *playerPhoto = strtok(NULL, ",");
        char *nationalityPlayer = strtok(NULL, ",");

        // skip 2 kolom (flag, overall)
        name = strtok(NULL, ",");
        name = strtok(NULL, ",");

        char *potentialVal = strtok(NULL, ",");
        char *playerClub = strtok(NULL, ",");

        /*
            Mengubah string umur dan potensi menjadi integer
            atoi = a string to integer        
        */ 
        int agePlayer = atoi(ageVal);
        int potentialPlayer = atoi(potentialVal);

        // print pemain sesuai kriteria
        if (agePlayer < 25 && potentialPlayer > 85 && strcmp(playerClub, "Manchester City") != 0) {

            // satuan karakter (identifier)
            printf("%-7s\t|   %-7d|\t   %-12s|\t   %-14s|\t   %-9d|\t   %-10s\t\n", playerName, agePlayer, playerPhoto, nationalityPlayer, potentialPlayer, playerClub);
        }
    }

    fclose(csvData);
}


int main () {

    // Mengunduh dataset
    printf("Memulai pengunduhan datasets....\n");
    getDatasets();
    printf("Unduhan telah selesai.\n");

    // Mengekstrak dataset
    printf("Mengekstrak file datasets....\n");
    extrDatasets();
    printf("Proses ekstrak telah selesai.\n");

    // Melakukan analisis pada data
    printf("Analisis data pemain dimulai....\n");
    dataAnalysis();
    printf("Analisis data pemain selesai, pemain ditemukan!\n");

    return 0;
}
