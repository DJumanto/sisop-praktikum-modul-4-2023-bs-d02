## Laporan Resmi Modul 4 - Kelompok D02

- Thoriq Afif Habibi (5025211154)
- Alfa Fakhrur Rizal Zaini (5025211214)
- Rafi Aliefian Putra Ramadhani (5025211234)
---------

## Soal 1

### Deskripsi Soal 
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

    a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

    kaggle datasets download -d bryanb/fifa-player-stats-database

    Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya.


    b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

    c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
    Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

    d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa  sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
    Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

    e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

Catatan: 
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.  

### Penyelesaian Soal

#### ***Poin A dan B***
- Membuat program `storage.c` untuk download dan extract datasets.
```R
void getDatasets() {
    int getStatus = system("kaggle datasets download -d bryanb/fifa-player-stats-database -p ./");

    if (getStatus != 0) {
        printf("Oops! Terjadi kesalahan, gagal mengunduh datasets.\n");

        exit(1);
    }
}

void extrDatasets() {
    int extrStatus = system("unzip ./fifa-player-stats-database.zip -d ./");

    if (extrStatus != 0) {
        printf("Oops! Terjadi kesalahan, gagal ekstrak datasets.\n");

        exit(1); 
    }
}
```
Berikut hasil dari run function tersebut :

![2func](https://user-images.githubusercontent.com/91828276/242004360-837738dd-54a7-42eb-8abe-c9756fc9e9cf.png)

- Melakukan pembacaan data file dengan *FILE to Pointer* kemudian membuka file dengan *fopen*
```R
void dataAnalysis() {
    FILE *csvData = fopen("./FIFA23_official_data.csv", "r");
    
    if (csvData == NULL) {
        printf("Gagal membuka file CSV\n");
        return;
    }
    

    printf("Nama\t\t|   Umur   |   \t\t\tURL Foto\t\t\t   |    Kewarganegaraan\t |    Potensi\t    |   \tKlub\t\t\n");
    printf("----------------------------------------------------------------------------------------\n");

```

- Melakukan filtering data dari file `FIFA23_official_data.csv` yang dibaca dengan mula-mula mendefinisikan baris data dan menghapus header data (hanya membaca isi data)
```R
// data baris csv
char dataLine[2000];

// remove header
fgets(dataLine, sizeof(dataLine), csvData);
```

- Melakukan perulangan pada data `FIFA23_official_data.csv` untuk mendapatkan data-data dari setiap baris data yang akan ditampilkan nantinya dengan membagi string `current_line` menjadi bagian-bagian terpisah dengan menggunakan tanda koma (`,`).

    - Pemanggilan pertama `strtok` menggunakan `current_line` sebagai argumen pertama akan membagi string menjadi bagian pertama sebelum koma pertama. Bagian ini akan dianggap sebagai `"Nama Pemain"` 
    - Pemanggilan kedua `strtok` menggunakan `NULL` sebagai argumen pertama akan melanjutkan pemrosesan string yang sama dari titik terakhir yang disimpan oleh `strtok` sebelumnya. Kali ini, bagian yang diambil adalah bagian setelah koma pertama, yang juga merupakan nama pemain.
    - Pemeriksaan `if (name == NULL)` dilakukan untuk memastikan bahwa nama pemain telah berhasil dibaca. Jika name `bernilai NULL`, artinya `tidak ada token yang ditemukan` setelah koma pertama, kemungkinan karena format file CSV tidak sesuai atau baris data tidak lengkap. Dalam kasus ini, memori yang dialokasikan untuk `current_line` akan dibebaskan menggunakan fungsi `free`, dan loop continue akan membuat program melanjutkan ke iterasi berikutnya.

```R
// looping data csv
while (fgets(dataLine, sizeof(dataLine), csvData)) {
    // Membaca nama pemain
    char *current_line = strdup(dataLine);

    // get token (pemain)
    char *name = strtok(current_line, ",");
    name = strtok(NULL, ",");

    if (name == NULL) {
        free(current_line);
        continue;
    }
```

- Melakukan filtering pada data kolom yang diambil dan data yang akan ditampilkan `(Nama Pemain, Umur Pemain, URL Foto, Kewarganegaraan Pemain, Potensi Pemain, KLub Pemain)`

```R
// var hasil nama
    char *playerName = strdup(name);

    // Membaca info pribadi pemain
    char *ageVal = strtok(NULL, ",");
    char *playerPhoto = strtok(NULL, ",");
    char *nationalityPlayer = strtok(NULL, ",");
```

- Membuat skip untuk 2 kolom data (karena tidak dibutuhkan dalam requirement data permasalahan) pada file `FIFA23_official_data.csv` yang kemudian dilanjut dengan kolom selanjutnya
```R
// 2 kolom (national, flag)
name = strtok(NULL, ",");
name = strtok(NULL, ",");

char *potentialVal = strtok(NULL, ",");
char *playerClub = strtok(NULL, ",");
```

- Mengubah tipe data `Umur Pemain` dan `Potensial Pemain` dari yang semula bertipe data *`string`* menjadi *`integer`*
```R
int agePlayer = atoi(ageVal);
int potentialPlayer = atoi(potentialVal);
```

- Mendapatkadan data nama pemain dengan ketentuan data pemain yang berusia `di bawah 25 tahun`, memiliki potensi `di atas 85`, dan bermain di klub lain `selain Manchester City` dan kemudian dicetak sesuai urutan kolom dengan mengukur jarak identifier.
```R
if (agePlayer < 25 && potentialPlayer > 85 && strcmp(playerClub, "Manchester City") != 0) {

    // satuan karakter (identifier)
    printf("%-7s\t|   %-7d|\t   %-12s|\t   %-14s|\t   %-9d|\t   %-10s\t\n", playerName, agePlayer, playerPhoto, nationalityPlayer, potentialPlayer, playerClub);
    }
}
```

- Menutup file `FIFA23_official_data.csv` yang telah dibaca dan dilakukan filtering
```R
fclose(csvData);
```

- Mencetak semua hasil dalam console terminal dalam fungsi `main`
```R
int main () {

    // Mengunduh dataset
    printf("Memulai pengunduhan datasets....\n");
    getDatasets();
    printf("Unduhan telah selesai.\n");

    // Mengekstrak dataset
    printf("Mengekstrak file datasets....\n");
    extrDatasets();
    printf("Proses ekstrak telah selesai.\n");

    // Melakukan analisis pada data
    printf("Analisis data pemain dimulai....\n");
    dataAnalysis();
    printf("Analisis data pemain selesai, pemain ditemukan!\n");

    return 0;
}
```

Berikut hasil nama-nama pemain yang sesuai dengan ketentuan pada program `storage.c` :

![res1](https://user-images.githubusercontent.com/91828276/242004379-62794e47-125b-4d90-a898-2e0b1dee09b0.png)

![res2](https://user-images.githubusercontent.com/91828276/242004389-f39a38d4-23a4-4a1e-97d7-584004db4bd2.png)

![res3](https://user-images.githubusercontent.com/91828276/242004396-37cf6d58-f969-4f8c-bab4-539b6f59a6de.png)


#### ***Poin C***
- Setelah melakukan penyusunan program `storage.c`, langkah selanjutnya yaitu menjadikan kedalam sebuah `Docker Container` agar dapat di-distribute dan dijalankan di *environtment* lain. Langkah awal yang perlu dilakukan adalah dengan membuat `Dockerfile` untuk pendefinisian instruksi-instruksi yang ada didalam program `storage.c`. Adapun isi dari `Dockerfile` nya sebagai berikut :
```R
FROM ubuntu:latest

RUN mkdir -p /sisop-mod-4/

WORKDIR /sisop-mod-4/

COPY . /sisop-mod-4/

RUN apt-get update && apt-get install -y gcc python3 python3-pip unzip

RUN pip3 install kaggle

COPY kaggle.json /root/.kaggle/kaggle.json
RUN chmod 600 /root/.kaggle/kaggle.json

RUN gcc -o storage storage.c

CMD ["./storage"]
```

- Selanjutnya, membuat `Docker Image` dengan menggunakan `Docker CLI (Command Line Interface)` yang dilakukan pada *console terminal* pada environtment user.

Berikut hasil dari build `Dockerfile` menjadi `Docker Image` :

**CLI Docker Image**
![cliDockImg](https://user-images.githubusercontent.com/91828276/242005604-671b3372-9dfb-4455-b7a6-67cedf9e02f0.png)
**Hasil build Docker Image**
![ResDockImg](https://user-images.githubusercontent.com/91828276/242005625-afb95532-26e4-44eb-83cf-3beacc32d3a5.png)

- Memeriksa verifikasi running dan output dari hasil `Docker Image` dengan dijalankan menjadi `Docker Container`
![ResDockCont](https://user-images.githubusercontent.com/91828276/242005611-ee7b9313-6eaf-4666-bcfc-265adb4e0997.png)

    **Exit(0) = Successed* 

#### ***Poin D***

- Setelah berhasil membuild program menjadi `Docker Image`, langkah selanjutnya yaitu mem-publish kedalam sistem `Docker Hub`yang dimana merupakan layanan cloud developer seperti *GitHub, GitLab*. Berikut hasilnya :

![ResDockHub](https://user-images.githubusercontent.com/91828276/242005617-d1e66f08-6276-41e6-8056-b6ec9d98e3c8.png)

#### ***Poin E***

- Setelah berhasil melakukan push kedalam `Docker Hub`, langkah selanjutnya yaitu membuat `Docker Compose` dengan menangkap data *image* dari *Docker Hub* untuk dapat dijalankan/diakses pada direktori lain maupun *environtment* lain. Pada permasalahan kali ini pembuatan *Docker Compose* dilakukan sebanyak *5 instance* dengan hasil *ports container* yang berbeda-beda dengan membuat dan dijalankan pada  direktori yang berbeda (`Barcelona` dan `Napoli`). Isi dari `Docker Compose` nya seperti berikut :
```R
version: '3'
services:
  # Container for Barcelona
  barcelona-storage-app-number:
    image: rafifiaan/storage-app:1.0.2
    ports:
      - "0:70"
    deploy:
      replicas: 5

  # Container for Napoli
  napoli-storage-app-number:
    image: rafifiaan/storage-app:1.0.2
    ports:
      - "0:90"
    deploy:
      replicas: 5

```

- **Barcelona** 

![cliDockComp1](https://user-images.githubusercontent.com/91828276/242007182-e2652258-0c66-4552-8869-b556551291ff.png)

![runDockComp1](https://user-images.githubusercontent.com/91828276/242007213-99a4b859-7951-48c6-893f-b3a0f014a6f1.png)

- **Napoli**

![cliDockComp2](https://user-images.githubusercontent.com/91828276/242007192-2f35e7c4-a7c0-4f00-ae21-9dc8d5c56e3e.png)

![runDockComp2](https://user-images.githubusercontent.com/91828276/242007215-221ec655-24c3-4478-a939-d9471aa29a54.png)

-----------------------

**Hasil Barcelona**

![resDockComp1](https://user-images.githubusercontent.com/91828276/242007201-87694ec5-2e44-4fd1-8e64-5dcd6f0d3033.png)

**Hasil Napoli**

![resDockComp1](https://user-images.githubusercontent.com/91828276/242007207-42777b62-54d6-442c-9c40-f784040a8d78.png)

### Kendala Penyelesaian
 - Sempat stuck untuk konfigurasi pada *Docker Compose*

 ## Soal 4

### Deskripsi Soal 
Pada soal ini, diperlukan sebuah program fuse yang dapat mencatat setiap kegiatan yang dilakukan pada direktori fuse, memecah file yang ada pada direktori modular, menormalkan file yang telah dipecah, dan mengembalikan file ke bentuk normal jika direktori diubah tidak modular. Berikut rincian properti yang harus dimiliki:
1. Pada filesystem tersebut, jika dibuat atau di-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
2. Saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
3. Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.
4. Saat melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
5. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

### Penyelesaian

#### Poin pertama dan kedua
Untuk mememenuhi kriteria pertama, dibutuhkan implementasi fuse pada fungsi xmp_mkdir dan xmp_rename yang akan mendeteksi apakah direktori berbentuk modular atau tidak. Detail kode sebagai berikut:
``` R
static int xmp_mkdir(const char *folder, mode_t md){
    int res;
    char dir[300];
    strcpy(dir, folder);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    if(strlen(dir)<7)
        res = mkdir(folder, md);

    else if(strncmp(dir,"module_",7)!=0)
        res = mkdir(folder, md);

    else{
        char modPath[400], modFolder[50], module[10];
        strcpy(module,"/module_");
        strcpy(dir, folder);
        strcpy(dir,dirname(dir));

        strcpy(modFolder, folder);
        strcpy(modFolder,strrchr(modFolder,'/')+1);
        sprintf(modPath,"%s%s%s",dir,module,modFolder);

        res = mkdir(modPath, md);
    }

    if (res==-1) return -errno;

    fuselog("REPORT","MKDIR",folder);

    return res;
}

static int xmp_rename(const char* path1, const char* path2){
    int res = rename(path1,path2);
    char dir1[100], dir2[100];
    strcpy(dir1, path1);
    strcpy(dir1, strrchr(dir1,'/')+1);
    strcpy(dir2, path2);
    strcpy(dir2, strrchr(dir2,'/')+1);

    if(res==-1) return -errno;

    if(strncmp(dir1,"module_",7)!=0 && strncmp(dir2,"module_",7)==0){
        char path[1000];
        strcpy(path,path2);
        modulardir(path);
    }

    else if(strncmp(dir1,"module_",7)==0 && strncmp(dir2,"module_",7)!=0){
        char path[1000];
        strcpy(path,path2);
        unmodulardir(path);
    }

    char desc[100];
    sprintf(desc,"%s::%s",path1,path2);
    fuselog("REPORT","RENAME",desc);

    return 0;
}
```
Pada fungsi mkdir, direktori masih kosong sehingga tidak perlu dilakukan apa-apa, hanya perlu melakukan system call mkdir sesuai dengan nama direktorinya. Jika direktori merupakan subdirektori dari direktori modular, tambahkan awalan "module_", hal tersebut sesuai dengan poin kedua. Pada rename, jika direktori menjadi modular, maka akan dipanggil fungsi modulardir() untuk melakukan modularisasi pada file dan folder di dalamnya.<br>
Seperti pada poin kedua, maka fungsi modulardir() harus melakukan directory listing secara rekursif, kemudian mengubah subdirektori menjadi berawalan module_, dan memecah file menjadi beberapa file. Fungsi modulardir() adalah sebagai berikut:
``` R
void modulardir(char *basePath){
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strncmp(dp->d_name, ".",1) != 0 && strncmp(dp->d_name, "..",2) != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if(isDir(path)){
                // cek apakah sudah modular
                if(strncmp(dp->d_name,"module_",7)!=0){
                    char mod_path[1000];
                    sprintf(mod_path,"%s/module_%s",basePath,dp->d_name);
                    rename(path,mod_path);
                    modulardir(mod_path);
                }
                else{
                    modulardir(path);
                }
            }

            else{
                struct stat *buf=(struct stat*)malloc(sizeof(struct stat));
                stat(path,buf);
                modularize(path,buf->st_size,buf->st_mode);

                free(buf);
            }
        }
    }

    closedir(dir);
}
```
Fungsi ini mengubah subdirektori yang belum modular menjadi berawalan modular dengan fungsi sprintf(). Isi ubdirektori tersebut kemudian juga perlu dimodularisasi dengan memanggil kembali fungsi modulardir(). fungsi ini juga akan memecah file dengan memanggil fungsi modularize(). Karena fungsi ini membutuhkan properti mode, maka perlu didapatkan atribut file-nya terlebih dahulu dengan fungsi stat().

#### Poin 3
Untuk memenuhi poin nomer 3, pertama diperlukan suatu fungsi yang dapat melakukan pencatatan kegiatan fuse pada file "/home/thoriqaafif/fs_module.log" dengan argumen level, cmd atau jenis system callnya, dan deskripsi. Implementasi fungsi ini adalah sebagai berikut:
``` R
void fuselog(char *level,char *cmd, const char *desc){
    FILE *fptr;
    fptr=fopen("/home/thoriqaafif/fs_module.log","a");

    time_t now;
    time(&now);
    struct tm *time=localtime(&now); 
    char strtime[20];
    strftime(strtime,20,"%y%m%d-%X",time);

    // fprintf(fptr,"%s::%s::%s::%s\n",level,strtime,cmd,desc);
    printf("%s::%s::%s::%s\n",level,strtime,cmd,desc);
    fclose(fptr);

    return;
}
```
Format log juga membutuhkan waktu sehingga digunakan time(), localtime(), dan strftime() untuk mendapatkan string waktu saat ini sesuai dengan format.<br>
Pencatatan ini harus dilakukan untuk seluruh system call sehingga program ini perlu mengimplementasikan seluruh operasi fuse, lalu memanggil fuselog() pada implementasi operasi fuse tersebut. Operasi fuse yang diimplementasikan diletakkan pada struct fuse_operations berikut:
``` R
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};
```
Salah satu contoh pemanggilan fuselog() pada getattr():
```R
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char dir[300];
    char fullPath[1000];
    int num=0;
    struct stat *st=(struct stat*)malloc(sizeof(struct stat));
    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);
    sprintf(fullPath,"%s.000",path);

    if(strncmp(dir,"module_",7)==0 && access(fullPath,F_OK)==0){
        res=lstat(fullPath,stbuf);
        if(res==-1) return -errno;
        stbuf->st_size=0;

        while(access(fullPath,F_OK)==0){
            res=lstat(fullPath,st);
            if(res==-1) return -errno;

            stbuf->st_size+=st->st_size;
            num++;
            sprintf(fullPath,"%s.00%d",path,num);
        }
        // stbuf->st_size=total_size;
        printf("STBUF->SIZE %lu\n",stbuf->st_size);
    }
    else{
        res = lstat(path,stbuf);
        if(res==-1) return -errno;
    }

    free(st);
    
    fuselog("REPORT","STAT",path);

    return 0;
}
```

#### Poin 4
Pada poin 1 dan 2, fungsi modulardir() memanggil fungsi modularize() untuk memecah file. Fungsi modularize() akan memecah menjadi 1024 bytes sesuai dengan ketentuan poin 4. Pemecahan file dilakukan dengan membaca bit file yang akan dipecah dengan maksimal 1024 bit, lalu membuat file baru (pecahannya) dan menulis bit yang telah dibaca sebelumnya ke dalam file baru. Hal tersebut akan diulang hingga seluruh bit pada file yang dipecah telah dibaca. Implementasi fungsi ini adalah sebagai berikut:
``` R
int modularize(const char *path, int size, mode_t mode){
    FILE* src;
    src=fopen(path,"rb");
    if(src==NULL) return -1;

    char buf[MAXSIZE+5];
    int bytesRead=0;
    int numfile=0;
    int res;
    bytesRead=fread(buf,1,MAXSIZE,src);

    while(bytesRead>0){
        char fileName[500];
        sprintf(fileName,"%s.00%d",path,numfile);
        res=creat(fileName,mode);
        if(res==-1) return-1;


        FILE* newFile;
        newFile=fopen(fileName,"wb");
        if(newFile==NULL) return -1;

        fwrite(buf,1,bytesRead,newFile);
        fclose(newFile);

        numfile++;
        bytesRead=fread(buf,1,MAXSIZE,src);
    }
    fclose(src);
    
    unlink(path);
    return 0;
}
``` 
Poin 4 juga meminta file yang telah terpecah tersebut akan kembali normal (tidak terpecah) jika diakses melalui fuse. Hal tersebut dapat dilakukan dengan memodifikasi operasi fuse readdir dan read. Pertama, readdir harus membaca file pecahan yang terdapat pada direktori modular sebagai satu kesatuan file. Karena harus dianggap sebagai satu kesatuan, maka hanya dipanggil fungsi filler() pada file pecahan pertama. Di program ini, pembacaan isi direktori menggunakan scandir dan alphasort sehingga dipastikan terurut berdasarkan namanya. Maka file pecahan pasti akan dibaca secara berurutan. Hal tersebut berguna untuk mengecek apakah file yang sedang dibaca sama dengan file sebelumnya. Jika berbeda, maka dapat dipastikan file tersebut adalah pecahan pertama. Dengan begitu, panggil fungsi filler() dan evaluasi prevFile (file sebelumnya). Implementasi operasi readdir adalah sebagai berikut:
``` R
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent **namelist;
    char *dirName;
    bool modular=false;
    char fileName[256];
    char prevFile[256];
    char currPath[1000];
    int numfile=0;
    (void) offset;
    (void) fi;

    strcpy(prevFile,"");
    dirName=strrchr(path, '/')+1;
    if(strncmp(dirName,"module_",7)==0) modular=true;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    numfile=scandir(path,&namelist,NULL,alphasort);

    if(numfile<0){
        closedir(dp);
        return -ENOENT;
    }

    for(int i=0; i<numfile; i++) {
        strcpy(currPath, path);
        strcat(currPath, "/");
        strcat(currPath, namelist[i]->d_name);
        struct stat *st=(struct stat *) malloc(sizeof(struct stat));
        stat(currPath,st);

        st->st_ino = namelist[i]->d_ino;
        st->st_mode = namelist[i]->d_type << 12;

        if(modular && S_ISREG(st->st_mode) && strncmp(namelist[i]->d_name, ".",1) != 0 && strncmp(namelist[i]->d_name, "..",2) != 0){
            strncpy(fileName,namelist[i]->d_name,strlen(namelist[i]->d_name)-4);
            fileName[strlen(namelist[i]->d_name)-4]='\0';
            if(strcmp(prevFile,fileName)!=0){
                if(filler(buf, fileName, st, 0)) break;
                strcpy(prevFile,fileName);
            }
        }
        else
            if(filler(buf, namelist[i]->d_name, st, 0)) break;

        free(st);
    }
    closedir(dp);

    fuselog("REPORT","READDIR",path);

    return 0;
}
```
Selain itu, fuse juga perlu mengubah cara pembacaan file untuk file modular. Read akan membaca satu per satu bit dari file pecahan dan di-append ke dalam buf. Nilai res (sebagai return value) juga perlu dievaluasi tiap pembacaan sesuai jumlah bit yang dibaca. Implementasi lengkap fungsi read adalah sebagai berikut:
``` R
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    printf("read\n%lu\n",size);
    int fd;
    int res=0;
    (void) fi;
    char dir[300];
    char mod_path[300];
    char *temp_buf=(char *)malloc(sizeof(char)*MAXSIZE);
    int cnt=0;
    int numBytes=0;

    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    // modular
    if(strncmp(dir,"module_",7)==0){
        strcpy(mod_path,path);
        strcat(mod_path,".000");

        strcpy(buf,"");
        while(access(mod_path,F_OK)==0){
            fd=open(mod_path,O_RDONLY);
            if(fd==-1) return -errno;

            printf("%s\n",mod_path);
            numBytes=pread(fd, temp_buf, MAXSIZE,offset);
            printf("file ke-%d berukuran%lu harusnya %d\n",cnt,strlen(temp_buf),res);
            strncat(buf,temp_buf,numBytes);
            res+=numBytes;
            cnt++;
            sprintf(mod_path,"%s.00%d",path,cnt);

            close(fd);
        }
        printf("%s\n",buf);
    }

    // tidak modular
    else{
        fd = open(path, O_RDONLY);

        if(fd==-1) return -errno;

        res = pread(fd, buf, size, offset);

        if (res == -1) res = -errno;
        close(fd);
    }

    // free(temp_buf);
    printf("READ dengan ukuran %lu\n",size);

    fuselog("REPORT","READ",path);

    return res;
}
``` 

#### Poin 5
Pada poin 5, diperlukan pengembalian direktori modular menjadi non-modular. Jika untuk merubah ke modular digunakan fungsi modulardir(), untuk mengembalikannya digunakan fungsi unmodulardir(). Fungsi ini bekerja secara terbalik, mengubah nama subdirektori menjadi tanpa module_ dan menyatukan kembali file dengan fungsi unmodularize. Berikut implementasi unmodulardir()
``` R
void unmodulardir(char *basePath){
    char path[1000];
    char prevPath[1000];
    strcpy(prevPath,path);
    int numfile=0;
    struct dirent **namelist;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    numfile=scandir(basePath,&namelist,NULL,alphasort);

    if(numfile<0){
        closedir(dir);
        return ;
    }

    for (int i=0;i<numfile;i++)
    {
        if (strncmp(namelist[i]->d_name, ".",1) != 0 && strncmp(namelist[i]->d_name, "..",2) != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, namelist[i]->d_name);

            if(isDir(path)){
                // cek apakah sudah modular
                if(strncmp(namelist[i]->d_name,"module_",7)==0){
                    char mod_path[1000],folder_name[100];
                    strcpy(folder_name,namelist[i]->d_name);
                    sprintf(mod_path,"%s/%s",basePath,&folder_name[7]);
                    rename(path,mod_path);
                    unmodulardir(mod_path);
                }
                else{
                    unmodulardir(path);
                }
            }

            else{
                char newPath[1000];
                struct stat *buf=(struct stat*)malloc(sizeof(struct stat));
                stat(path,buf);
                strncpy(newPath,path,strlen(path)-4);
                newPath[strlen(path)-4]='\0';
                if(strcmp(prevPath,newPath)!=0){
                    strcpy(prevPath,newPath);
                    unmodularize(newPath,buf->st_mode);
                }

                free(buf);
            }
        }
    }

    closedir(dir);
}
```
Selanjutnya, fungsi unmodularize akan membuat file dengan nama sesuai nama utuh filenya. Selanjutnya, file pecahan akan dibaca tiap bitnya dan ditulis ke file yang telah dibuat. Berikut implementasinya:
``` R
int unmodularize(const char *path, mode_t mode){
    int res = creat(path,mode);
    if(res==-1) return -1;

    FILE* src;
    src=fopen(path,"ab");
    if(src==NULL) return -1;

    char buf[MAXSIZE+5],fileName[500];
    int bytesRead=0;
    int numfile=0;
    sprintf(fileName,"%s.00%d",path,numfile);

    while(access(fileName,F_OK)==0){
        FILE* fl;
        fl=fopen(fileName,"rb");
        if(fl==NULL) return -1;

        bytesRead=fread(buf,1,MAXSIZE,fl);

        fwrite(buf,1,bytesRead,src);
        fclose(fl);
        unlink(fileName);

        numfile++;
        sprintf(fileName,"%s.00%d",path,numfile);
    }
    fclose(src);
    
    return 0;
}
```

### Hasil
1. Melakukan modularisasi
    - pada direktori asli<br>
    ![1a](https://github.com/Thoriqaafif/image/blob/main/Modularisasi%20direktori%20asli.png?raw=true)

    - pada fuse<br>
    ![1b](https://github.com/Thoriqaafif/image/blob/main/modularisasi_fuse.png?raw=true)

2. Membaca file modular pada fuse<br>
![2](https://github.com/Thoriqaafif/image/blob/main/membaca%20file%20modular%20pada%20fuse.png?raw=true)

3. Hasil file log<br>
![3](https://github.com/Thoriqaafif/image/blob/main/Screenshot%20from%202023-06-03%2014-04-29.png?raw=true)

4. Mengembalikan direktori modular<br>
    - rename modular direktori menjadi non modular pada fuse<br>
    ![4a](https://github.com/Thoriqaafif/image/blob/main/ubah%20ke%20non%20modular%20fuse.png?raw=true)

    - direktori kembali seperti sebulam pada direktori asli<br>
    ![4b](https://github.com/Thoriqaafif/image/blob/main/ubah%20ke%20non%20modular%20fuse.png?raw=true)

### Kendala Penyelesaian
- belum familiar dengan cara kerja file system
- implementasi operasi fuse sangat banyak

## Soal 5

Setelah sukses menjadi pengusaha streaming musik di chapter kehidupan sebelumnya, Elshe direkrut oleh lembaga rahasia untuk membuat sistem rahasia yang terenkripsi.  Kalian perlu membantu Elshe dan membuat program rahasia.c. Pada program rahasia.c, terdapat beberapa hal yang harus kalian lakukan sebagai berikut.					
- Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.
- Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.
- Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
Contoh:
kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
Penjelasan:
kyunkyun adalah username.
fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
Catatan: 
Tidak perlu ada password validation untuk mempermudah kalian.		
Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.

Contoh:
- A01_a.pdf
- xP4UcxRZE5_A01


- List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.


**extension.txt:**

- folder = 12
- jpg = 13
- png = 2
- .
- .
- .
- dan seterusnya

### Penyelesaian:

Pada program rahasia.c, kita cukup memanfaatkan library fuse yang ada, dan hanya perlu memodifikasi pada bagian xmp_readdir, membuat progrm rekursi untuk membaca file, menambahkan xmp_open untuk rename, dan membaut sistem login. Berikut modifikasi yang ditambahkan pada program yang dibuat:

**extension counter**
```C
void extCount(const char *pathh) {
    DIR *dir = opendir(pathh);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", pathh);
        return;
    }
 
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
 
        char entryPath[1000];
        snprintf(entryPath, sizeof(entryPath), "%s/%s", pathh, entry->d_name);
 
        struct stat st;
        if (stat(entryPath, &st) == -1) {
            printf("Failed to get file information for: %s\n", entryPath);
            continue;
        }
        if (S_ISREG(st.st_mode)) {
            if (strstr(entry->d_name, "pdf") != NULL) {
                pdf++;
            } 
 
            else if (strstr(entry->d_name, "jpg") != NULL) {
                jpg++;
            } 
 
            else if (strstr(entry->d_name, "txt") != NULL) {
                txt++;
            } 
 
            else if (strstr(entry->d_name, "mp3") != NULL) {
                mp3++;
            } 
 
            else if (strstr(entry->d_name, "png") != NULL) {
                png++;
            } 
 
            else if (strstr(entry->d_name, "docx") != NULL) {
                docx++;
            } 
 
            else if (strstr(entry->d_name, "gif") != NULL) {
                gif++;
            } 
 
            else {
                printf("Extension tidak dikenali: %s\n", entry->d_name);
            }
        }
        else if (S_ISDIR(st.st_mode)) {
            folder++;
            extCount(entryPath);
        }
    }
 
    closedir(dir);
}
```

**Recursive Renaming**:
```C
void DirRecursive(char fpath[2000]){
    printf("I\'m Here %s\n",fpath);	
    DIR *dp;
    struct dirent *de;
    char temporary[3000];
    sprintf(temporary,"%s/",fpath);
    dp = opendir(temporary);

    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
	printf("\nrenaming all inside %s folder\n",de->d_name);
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

	if(strcmp(de->d_name, ".") !=0 && strcmp(de->d_name, "..") != 0){

        char filepath[PATH_MAX];
        snprintf(filepath, PATH_MAX, "%s/%s", fpath, de->d_name);
	
	if(stat(filepath,&st) == -1){
                return;
        }

          if(strstr(de->d_name,"d02") == NULL){
            if(S_ISDIR(st.st_mode)){
                snprintf(filepath, PATH_MAX, "%s/%s", fpath, de->d_name);
		printf("\n\nWOW ANOTHER DIR\n\n");
		DirRecursive(filepath);
		
            }
           if(S_ISDIR(st.st_mode)){
               printf("about to rename dirname: %s\n",de->d_name);
                char temp[2000];
                char oldpath[2000];
                sprintf(temp,"%s/%s_d02",fpath,de->d_name);
                sprintf(oldpath,"%s/%s",fpath,de->d_name);
                int status = rename(oldpath,temp);
                if(status) printf("failed to rename %s\n", de->d_name);
		else printf("HERE WE GO  %s\n",temp);
             }
            else if(S_ISREG(st.st_mode)){
		printf("about to rename filename: %s\n",de->d_name);
                char temp[2000];
                char oldpath[2000];
                sprintf(temp,"%s/d02_%s",fpath,de->d_name);
                sprintf(oldpath,"%s/%s",fpath,de->d_name);
                int status = rename(oldpath,temp);
                if(status) printf("failed to rename %s\n", de->d_name);
		        else printf("HERE WE GO %s\n",temp);
             }
           }
        }
        //res = (filler(buf, de->d_name, &st, 0));
        //if(res!=0) break;
    }

    closedir(dp);

    return;
}
```

**Tree creator**
```C
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void) fi;
 
    int fd = open("/home/djumanto/sisop/testfuse/result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd == -1) {
        perror("Error opening file");
        return -1;
    }
 
    int stdout_out = dup(STDOUT_FILENO);

    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting stdout");
        close(fd);
        return -1;
    }
 
    char command[1024];
    snprintf(command, sizeof(command), "tree %s", dirpath);
 
    system(command);

    dup2(stdout_out, STDOUT_FILENO);
    close(stdout_out);
 
    close(fd);
 
    return 0;
}
```
**login and register system**

**login**
```C
void login(){
        char name[200];
        char pass[200];
        printf("[+] Login Page\n");
        printf("Please insert your name!\n");
        printf("> ");
        scanf("%s",name);

        printf("Please insert your password!\n");
        printf("> ");
        scanf("%s",pass);

        int avail = checkcred(name);

        if(avail){
          unsigned char hash[MD5_DIGEST_LENGTH];
          char hashStr[MD5_DIGEST_LENGTH * 2 + 1];
          calculateMD5(pass,hash);
          digestToString(hash,hashStr);
          int passcheck = checkcred(hashStr);
          if(passcheck){
	    printf("Welcome User %s!\n", name);
           }
	}

        else{
          printf("There is no user such %s, please register!\n",name);
          exit(1);
        }
}
```

**Register**
```C
void regist(){
	char name[200];
	char pass[200];
	printf("[+] Register Page\n");
	printf("Please insert your name!\n");
	printf("> ");
	scanf("%s",name);

	printf("Please insert your password!\n");
        printf("> ");
	scanf("%s",pass);

	int avail = checkcred(name);

	if(!avail){
	  printf("[+] Registering new user\n");
	  unsigned char hash[MD5_DIGEST_LENGTH];
	  char hashStr[MD5_DIGEST_LENGTH * 2 + 1];
	  calculateMD5(pass,hash);
	  digestToString(hash,hashStr);
	  FILE *userList = fopen("userList.txt","a");
	  fprintf(userList,"%s %s\n",name,hashStr);
	  printf("[+] User %s has been registered, please login\n", name);
	  fclose(userList);
	  login();
	}
	else{
	 printf("User named %s is already registered, please login!\n",name);
	 exit(1);
	}
}
```

pada system login tersebut, saya memanfaatkan external library opensll untuk melakukan md5 hashing dan menyimpan digestnya ke userList:

```txt
ARIFIN 25d55ad283aa400af464c76d713c07ad
DJUMANTO eed383b0fef37a8ed571200d39782e89
Aldo 2ad89d1422b9bca74f3612d6f130c3a0
```

Berikut adalah docker-compose untuk membuild container:
```yml
version: "3"
services:
  rahasiaapp:
    build:
      context: .
      dockerfile: Dockerfile
    image: rahasia_di_docker_d02
    container_name: container_d02
    command: tail -f /dev/null
    volumes:
      - /home/djumanto/sisop/testfuse/Rahasia/rahasia:/usr/share
```

Berikut adalah hasil dari result.txt
```txt
/home/djumanto/sisop/testfuse/Rahasia/rahasia
├── 80T86jn7xh_d02
│   ├── d02_elaine-casap-82gJggDId-U-unsplash.jpg
│   └── d02_tobias-tullius-3BcSyNMJ2WM-unsplash.jpg
├── AG5zGHsasr_d02
│   └── d02_File-iVVm9.txt
├── d02_b.pdf
├── d02_carles-rabada-kMde0v9tYYM-unsplash.jpg
├── d02_File-49ynD.txt
├── d02_File-Oadk3.txt
├── d02_File-PdCim.txt
├── d02_funny-eastern-short-music-vlog-background-hip-hop-beat-29-sec-148905.mp3
├── d02_mygamer.gif
├── d02_png-transparent-construction-equipment-illustrations-heavy-machinery-architectural-engineering-truck-vehicle-construction-sites-icon-material-free-free-logo-design-template-building-text-thumbnail.png
├── dU87gUYIBN_d02
│   ├── d02_cinematic-logo-strong-and-wild-142807.mp3
│   ├── d02_clapping-opener-rhythmic-original-dynamic-promo-114789.mp3
│   ├── d02_mohamed-ahsan-cjkgrFt_6bE-unsplash.jpg
│   ├── d02_motivational-corporate-short-110681.mp3
│   ├── d02_png-transparent-free-download-icon-cloud-down-computer-system-blue-computer-blue-laptop-thumbnail.png
│   └── d02_sergey-zhesterev-lutEBBTpkeE-unsplash.jpg
├── Dyy2q9B0Ax_d02
│   ├── d02_File-CxD3o.txt
│   └── d02_patrick-hendry-jS0ysot7MwE-unsplash.jpg
├── FH2CQo5o8z_d02
│   ├── d02_File-dUnQq.txt
│   ├── d02_File-QDJhp.txt
│   ├── d02_File-uuXxv.txt
│   ├── d02_png-transparent-zombie-download-with-transparent-background-fantasy-free-thumbnail.png
│   └── d02_randall-ruiz-LVnJlyfa7Zk-unsplash.jpg
├── gGJ5r0Df82_d02
│   ├── d02_andre-tan-3SK2x-kbMSI-unsplash.jpg
│   ├── d02_andrii-ganzevych-IVlVHXgvi0Y-unsplash.jpg
│   ├── d02_carles-rabada-L7J0q5N7LjQ-unsplash.jpg
│   ├── d02_File-aBxnM.txt
│   ├── d02_File-EBdiV.txt
│   ├── d02_File-OlZ8w.txt
│   └── d02_File-XZJco.txt
├── IKJHSAU9731289dsa_d02
│   └── d02_png-transparent-mobile-phone-repair-material-mobile-phone-repair-phone-iphone-thumbnail.png
├── IzvxA5vw2U_d02
│   ├── d02_kris-mikael-krister-aGihPIbrtVE-unsplash.jpg
│   ├── d02_png-transparent-video-production-freemake-video-er-video-icon-free-angle-text-rectangle-thumbnail.png
│   └── d02_the-devilx27s-entry-143138.mp3
├── m4xOZ1lmux_d02
│   ├── d02_bharat-patil-_kPuQcU8C-A-unsplash.jpg
│   ├── d02_File-4qjlL.txt
│   ├── d02_File-uNOZN.txt
│   ├── d02_jon-tyson-Fz2BWfLDvGI-unsplash.jpg
│   └── d02_png-transparent-yellow-fire-fire-flame-fire-photography-orange-flame-thumbnail.png
├── mBFLP3XQMv_d02
│   ├── d02_epicaly-short-113909.mp3
│   ├── d02_File-6tNX4.txt
│   ├── d02_jcob-nasyr-uGPBqF1Yls0-unsplash.jpg
│   ├── d02_j.docx
│   ├── d02_ramona-flwrs-uJjhnN2WQJs-unsplash.jpg
│   ├── d02_redd-f-t8ts5bNQyWo-unsplash.jpg
│   └── d02_sami-takarautio-JiqalEW6Ml0-unsplash.jpg
├── nkSVqqpV7o_d02
│   ├── d02_c.pdf
│   ├── d02_d.pdf
│   ├── d02_epic-sport-clap-ampampamp-loop-main-9900.mp3
│   ├── d02_File-7Yf0S.txt
│   ├── d02_File-p6ZH2.txt
│   ├── d02_File-xBUoT.txt
│   ├── d02_k-on-yui-hirasawa.gif
│   ├── d02_matthew-feeney-L35x5fU07hY-unsplash.jpg
│   ├── d02_simple-logo-149190.mp3
│   ├── d02_thomas-owen-Y8vui_5mdto-unsplash.jpg
│   └── d02_upbeat-future-bass-opener-136069.mp3
├── Pp9LXs5FkR_d02
│   ├── d02_clapping-music-for-typographic-video-version-2-112975.mp3
│   ├── d02_File-UWKAJ.txt
│   ├── d02_File-X8lyf.txt
│   ├── d02_honkai-impact3rd-cute.gif
│   └── d02_wexor-tmg-L-2p8fapOA8-unsplash.jpg
├── SaEixBbm6y_d02
│   ├── d02_g.pdf
│   ├── d02_haikyuu-anime.gif
│   └── d02_h.pdf
├── uuXVhCbP3X_d02
│   ├── d02_a.pdf
│   ├── d02_e.pdf
│   ├── d02_epic-background-music-for-video-dramatic-hip-hop-24-seconds-149629.mp3
│   ├── d02_png-transparent-blue-dna-illustration-dna-3d-rendering-3d-computer-graphics-free-to-pull-dna-material-3d-computer-graphics-free-logo-design-template-photography-thumbnail.png
│   └── d02_png-transparent-tableware-plate-white-ceramic-plate-platter-dishware-download-with-transparent-background-thumbnail.png
├── VOpdACXtF3_d02
│   ├── d02_heart-ritsu-tainaka.gif
│   ├── d02_png-transparent-moon-moon-image-file-formats-nature-moon-png-thumbnail.png
│   └── d02_png-transparent-snowflake-light-snowflake-free-glass-free-logo-design-template-symmetry-thumbnail.png
├── vZAZecJwKF_d02
│   ├── d02_i.docx
│   ├── d02_png-transparent-kiwifruit-kiwi-free-fruit-kiwi-s-watercolor-painting-image-file-formats-food-thumbnail.png
│   ├── d02_png-transparent-sun-the-sun-sunscreen-light-sphere-sun-image-file-formats-orange-sphere-thumbnail.png
│   └── d02_tango-background-hip-hop-music-for-video-29-seconds-149879.mp3
├── xP4UcxRZE5_d02
│   ├── d02_energetic-background-reggaeton-short-music-27-sec-fun-vlog-music-149384.mp3
│   ├── d02_f.pdf
│   ├── d02_fun-background-hip-hop-short-music-27-sec-energetic-vlog-music-148916.mp3
│   ├── d02_kiss-hug.gif
│   ├── d02_png-transparent-pearl-material-sphere-pearl-balloon-download-with-transparent-background-free-thumbnail.png
│   ├── d02_png-transparent-yellow-fireworks-illustration-fireworks-pyrotechnics-free-to-pull-the-material-fireworks-s-free-logo-design-template-holidays-poster-thumbnail.png
│   └── d02_sirin-honkai-impact.gif
└── ZlzqBmSYbE_d02
    ├── d02_File-nENTg.txt
    ├── d02_File-NWnoK.txt
    ├── d02_File-xnlpH.txt
    ├── d02_png-transparent-bubble-illustration-water-drop-rain-drops-angle-white-text-thumbnail.png
    └── d02_png-transparent-golden-pistol-guns-pistol-free-download-pistol-firearms-thumbnail.png

18 directories, 90 files
```

Hasil dari extension.txt

```txt
folder: 18
mp3: 14
docx: 2
gif: 7
txt: 23
png: 16
jpg: 20
pdf: 8
```