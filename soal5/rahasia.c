#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>
#include <openssl/evp.h>
#include <sys/wait.h>
#include <openssl/md5.h>

static  const  char *dirpath = "/home/djumanto/sisop/testfuse/Rahasia/rahasia";
int jpg;
int txt;
int mp3;
int gif;
int folder;
int docx;
int pdf;
int png;

void extCount(const char *pathh) {
    DIR *dir = opendir(pathh);
    if (dir == NULL) {
        printf("Failed to open directory: %s\n", pathh);
        return;
    }
 
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }
 
        char entryPath[1000];
        snprintf(entryPath, sizeof(entryPath), "%s/%s", pathh, entry->d_name);
 
        struct stat st;
        if (stat(entryPath, &st) == -1) {
            printf("Failed to get file information for: %s\n", entryPath);
            continue;
        }
        if (S_ISREG(st.st_mode)) {
            if (strstr(entry->d_name, "pdf") != NULL) {
                pdf++;
            } 
 
            else if (strstr(entry->d_name, "jpg") != NULL) {
                jpg++;
            } 
 
            else if (strstr(entry->d_name, "txt") != NULL) {
                txt++;
            } 
 
            else if (strstr(entry->d_name, "mp3") != NULL) {
                mp3++;
            } 
 
            else if (strstr(entry->d_name, "png") != NULL) {
                png++;
            } 
 
            else if (strstr(entry->d_name, "docx") != NULL) {
                docx++;
            } 
 
            else if (strstr(entry->d_name, "gif") != NULL) {
                gif++;
            } 
 
            else {
                printf("Extension tidak dikenali: %s\n", entry->d_name);
            }
        }
        else if (S_ISDIR(st.st_mode)) {
            folder++;
            extCount(entryPath);
        }
    }
 
    closedir(dir);
}
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}


void DirRecursive(char fpath[2000]);

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

	//rename file and directory

	if(strcmp(de->d_name, ".") !=0 && strcmp(de->d_name, "..") != 0){
	
	char filepath[PATH_MAX];
	snprintf(filepath, PATH_MAX, "%s/%s", fpath, de->d_name);
	printf("%s\n",filepath);	

	if(stat(filepath,&st) == -1){
		return --errno;
	}
	  if(strstr(de->d_name,"d02") == NULL){
	    if(S_ISDIR(st.st_mode)){
		printf("\n\nIt\'s a directory!!!!\n\n");
	    	DirRecursive(filepath);
	    }
	    if(S_ISDIR(st.st_mode)){
		printf("dirname: %s\n",de->d_name);
		char temp[2000];
		char oldpath[2000];
		sprintf(temp,"%s/%s_d02",fpath,de->d_name);
		sprintf(oldpath,"%s/%s",fpath,de->d_name);
		int status = rename(oldpath,temp);
		if(status) printf("failed to rename %s\n", de->d_name);
		char anothertemp[3000];
		sprintf(anothertemp,"ls %s > /dev/null",temp);
	    	system(anothertemp);
		res = xmp_readdir(oldpath,buf,filler,offset,fi);
	    }
	    else if(S_ISREG(st.st_mode)){

		if(strstr(de->d_name, ".")!=NULL){
		printf("flename: %s\n",de->d_name);
		char temp[2000];
		char oldpath[2000];
                sprintf(temp,"%s/d02_%s",fpath,de->d_name);
                sprintf(oldpath,"%s/%s",fpath,de->d_name);
		if(access(temp,F_OK)==-1){	
			int status = rename(oldpath,temp);
			if(status) printf("failed to rename %s\n", de->d_name);
	     	  	}
		}
	    }
	   }
	}
	res = (filler(buf, de->d_name, &st, 0));
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}


void DirRecursive(char fpath[2000]){
    printf("I\'m Here %s\n",fpath);	
    DIR *dp;
    struct dirent *de;
    char temporary[3000];
    sprintf(temporary,"%s/",fpath);
    dp = opendir(temporary);

    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;
	printf("\nrenaming all inside %s folder\n",de->d_name);
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

	if(strcmp(de->d_name, ".") !=0 && strcmp(de->d_name, "..") != 0){

        char filepath[PATH_MAX];
        snprintf(filepath, PATH_MAX, "%s/%s", fpath, de->d_name);
	
	if(stat(filepath,&st) == -1){
                return;
        }

          if(strstr(de->d_name,"d02") == NULL){
            if(S_ISDIR(st.st_mode)){
                snprintf(filepath, PATH_MAX, "%s/%s", fpath, de->d_name);
		printf("\n\nWOW ANOTHER DIR\n\n");
		DirRecursive(filepath);
		
            }
           if(S_ISDIR(st.st_mode)){
               printf("about to rename dirname: %s\n",de->d_name);
                char temp[2000];
                char oldpath[2000];
                sprintf(temp,"%s/%s_d02",fpath,de->d_name);
                sprintf(oldpath,"%s/%s",fpath,de->d_name);
                int status = rename(oldpath,temp);
                if(status) printf("failed to rename %s\n", de->d_name);
		else printf("HERE WE GO  %s\n",temp);
             }
            else if(S_ISREG(st.st_mode)){
		printf("about to rename filename: %s\n",de->d_name);
                char temp[2000];
                char oldpath[2000];
                sprintf(temp,"%s/d02_%s",fpath,de->d_name);
                sprintf(oldpath,"%s/%s",fpath,de->d_name);
                int status = rename(oldpath,temp);
                if(status) printf("failed to rename %s\n", de->d_name);
		        else printf("HERE WE GO %s\n",temp);
             }
           }
        }
        //res = (filler(buf, de->d_name, &st, 0));
        //if(res!=0) break;
    }

    closedir(dp);

    return;

}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDWR);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *oldpath, const char *newpath)
{
    printf("renaming %s\n",oldpath);
    char f_oldpath[1000];
    char f_newpath[1000];

    sprintf(f_oldpath, "%s%s", dirpath, oldpath);
    sprintf(f_newpath, "d02_%s%s", dirpath, newpath);

    int res = rename(f_oldpath, f_newpath);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void) fi;
 
    int fd = open("/home/djumanto/sisop/testfuse/result.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fd == -1) {
        perror("Error opening file");
        return -1;
    }
 
    int stdout_out = dup(STDOUT_FILENO);

    if (dup2(fd, STDOUT_FILENO) == -1) {
        perror("Error redirecting stdout");
        close(fd);
        return -1;
    }
 
    char command[1024];
    snprintf(command, sizeof(command), "tree %s", dirpath);
 
    system(command);

    dup2(stdout_out, STDOUT_FILENO);
    close(stdout_out);
 
    close(fd);
 
    return 0;
}


static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
    .open = xmp_open,
};


void DownloadZip(const char* outputRelPath, const char* url){
	char cmd[2000];
	printf("[+] Download file from %s\n", url);
	sprintf(cmd, "wget \"%s\" -O \"%s\" --no-check-certificate", url, outputRelPath);
	int status = system(cmd);
	if(status == 0){
		printf("[+] Success download file\n");
	}else{
		printf("[-] There is error in downloading file\n");
		fprintf(stderr,"exiting");
		exit(1);
	}

}

void DecompressZip(const char* relPath){

	char cmd[256];
	system("ls");
	sprintf(cmd, "unzip %s -d Rahasia",relPath); 
	int status = system(cmd);

	if(status == 0){
                printf("[+] Success decompress file\n");
		system("rm rahasia.zip");
        }else{
                printf("[-] There is error in decompressing file\n");
                fprintf(stderr, "exiting");
                exit(1);
        }

}


void calculateMD5(const char *str, unsigned char *digest) {
    EVP_MD_CTX *mdctx;
    const EVP_MD *md;

    md = EVP_md5();
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit(mdctx, md);
    EVP_DigestUpdate(mdctx, str, strlen(str));
    EVP_DigestFinal(mdctx, digest, NULL);
    EVP_MD_CTX_free(mdctx);
}


void digestToString(const unsigned char *digest, char *str) {
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
        sprintf(&str[i * 2], "%02x", digest[i]);
    }
}


int checkcred(const char* cred){
	FILE* userList = fopen("userList.txt","r");

	if(!userList){
	   perror("fopen");
	} 

	char line[256];
    	while (fgets(line, sizeof(line), userList) != NULL) {
        if (strstr(line, cred) != NULL) {
            fclose(userList);
            return 1;
            }
    	}

	fclose(userList);

	return 0;
}

void login(){
        char name[200];
        char pass[200];
        printf("[+] Login Page\n");
        printf("Please insert your name!\n");
        printf("> ");
        scanf("%s",name);

        printf("Please insert your password!\n");
        printf("> ");
        scanf("%s",pass);

        int avail = checkcred(name);

        if(avail){
          unsigned char hash[MD5_DIGEST_LENGTH];
          char hashStr[MD5_DIGEST_LENGTH * 2 + 1];
          calculateMD5(pass,hash);
          digestToString(hash,hashStr);
          int passcheck = checkcred(hashStr);
          if(passcheck){
	    printf("Welcome User %s!\n", name);
           }
	}

        else{
          printf("There is no user such %s, please register!\n",name);
          exit(1);
        }
}


void regist(){
	char name[200];
	char pass[200];
	printf("[+] Register Page\n");
	printf("Please insert your name!\n");
	printf("> ");
	scanf("%s",name);

	printf("Please insert your password!\n");
        printf("> ");
	scanf("%s",pass);

	int avail = checkcred(name);

	if(!avail){
	  printf("[+] Registering new user\n");
	  unsigned char hash[MD5_DIGEST_LENGTH];
	  char hashStr[MD5_DIGEST_LENGTH * 2 + 1];
	  calculateMD5(pass,hash);
	  digestToString(hash,hashStr);
	  FILE *userList = fopen("userList.txt","a");
	  fprintf(userList,"%s %s\n",name,hashStr);
	  printf("[+] User %s has been registered, please login\n", name);
	  fclose(userList);
	  login();
	}
	else{
	 printf("User named %s is already registered, please login!\n",name);
	 exit(1);
	}


}

int main(int argc, char* argv[]){

	char *path = "rahasia.zip";
	char *url = "https://drive.google.com/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download&confirm=t&uuid=aed8b353-0a88-4573-89b8-b692d00aefc5";
	system("chmod 777 userList.txt");
	int flag;
	do{
	flag = 1;
	printf("Welcome, what do you want to do?\n");
        printf("> ");

        char com[10];
        scanf("%s", com);

        if(strcmp("login",com)==0)login();
        else if(strcmp("register",com)==0)regist();
	else flag = 0;
	}while(!flag);

	DownloadZip(path, url);
	DecompressZip(path);
    extCount(dirpath);
	sleep(8);
	pid_t pid = fork();
	if(pid == 0){
		fuse_main(argc, argv, &xmp_oper, NULL);
	}
	else{
		sleep(10);
		int status = system("sudo docker compose up -d");
		if(status==0) {
            char createTree[100];
            sprintf(createTree, "tree %s > result.txt",dirpath);
            system(createTree);
            FILE* extensionFile = fopen("extension.txt","a");
            if(extensionFile != NULL){
                printf("Inserting extension data\n");
                fprintf(extensionFile, "folder: %d\n", folder);
                fprintf(extensionFile, "mp3: %d\n", mp3);
                fprintf(extensionFile, "docx: %d\n", docx);
                fprintf(extensionFile, "gif: %d\n", gif);
                fprintf(extensionFile, "txt: %d\n", txt);
                fprintf(extensionFile, "png: %d\n", png);
                fprintf(extensionFile, "jpg: %d\n", jpg);
                fprintf(extensionFile, "pdf: %d\n", pdf);
                fclose(extensionFile);
            }
            else {
                perror("error opening file");
                return -1;
            }
            system("cat extension.txt");
		    system("sudo docker exec -it container_d02 /bin/bash -c 'ls /usr/share'"); 
        }
	}
	return 0;
}
