#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <string.h>
#include <libgen.h>
#define MAXSIZE 1024

void fuselog(char *level,char *cmd, const char *desc){
    FILE *fptr;
    fptr=fopen("/home/thoriqaafif/fs_module.log","a");

    time_t now;
    time(&now);
    struct tm *time=localtime(&now); 
    char strtime[20];
    strftime(strtime,20,"%y%m%d-%X",time);

    fprintf(fptr,"%s::%s::%s::%s\n",level,strtime,cmd,desc);
    fclose(fptr);

    return;
}

// memecah file
int modularize(const char *path, int size, mode_t mode){
    FILE* src;
    src=fopen(path,"rb");
    if(src==NULL) return -1;

    char buf[MAXSIZE+5];
    int bytesRead=0;
    int numfile=0;
    int res;
    bytesRead=fread(buf,1,MAXSIZE,src);

    while(bytesRead>0){
        char fileName[500];
        sprintf(fileName,"%s.00%d",path,numfile);
        res=creat(fileName,mode);
        if(res==-1) return-1;


        FILE* newFile;
        newFile=fopen(fileName,"wb");
        if(newFile==NULL) return -1;

        fwrite(buf,1,bytesRead,newFile);
        fclose(newFile);

        numfile++;
        bytesRead=fread(buf,1,MAXSIZE,src);
    }
    fclose(src);
    
    unlink(path);
    return 0;
}

// menggabungkan kembali file yang telah dipecah
int unmodularize(const char *path, mode_t mode){
    int res = creat(path,mode);
    if(res==-1) return -1;

    FILE* src;
    src=fopen(path,"ab");
    if(src==NULL) return -1;

    char buf[MAXSIZE+5],fileName[500];
    int bytesRead=0;
    int numfile=0;
    sprintf(fileName,"%s.00%d",path,numfile);

    while(access(fileName,F_OK)==0){
        FILE* fl;
        fl=fopen(fileName,"rb");
        if(fl==NULL) return -1;

        bytesRead=fread(buf,1,MAXSIZE,fl);

        fwrite(buf,1,bytesRead,src);
        fclose(fl);
        unlink(fileName);

        numfile++;
        sprintf(fileName,"%s.00%d",path,numfile);
    }
    fclose(src);
    
    return 0;
}

// check wether a path is directory or file
bool isDir(char path[])
{
    struct dirent *dp;
    DIR *dir = opendir(path);

    if (!dir)
        return false;

    if ((dp = readdir(dir)) != NULL)
    {
        return true;
    }

    return false;
}

// convert a directory to modular
void modulardir(char *basePath){
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strncmp(dp->d_name, ".",1) != 0 && strncmp(dp->d_name, "..",2) != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            if(isDir(path)){
                // cek apakah sudah modular
                if(strncmp(dp->d_name,"module_",7)!=0){
                    char mod_path[1000];
                    sprintf(mod_path,"%s/module_%s",basePath,dp->d_name);
                    rename(path,mod_path);
                    modulardir(mod_path);
                }
                else{
                    modulardir(path);
                }
            }

            else{
                struct stat *buf=(struct stat*)malloc(sizeof(struct stat));
                stat(path,buf);
                modularize(path,buf->st_size,buf->st_mode);

                free(buf);
            }
        }
    }

    closedir(dir);
}

// convert a directory to unmodular
void unmodulardir(char *basePath){
    char path[1000];
    char prevPath[1000];
    strcpy(prevPath,path);
    int numfile=0;
    struct dirent **namelist;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    numfile=scandir(basePath,&namelist,NULL,alphasort);

    if(numfile<0){
        closedir(dir);
        return ;
    }

    for (int i=0;i<numfile;i++)
    {
        if (strncmp(namelist[i]->d_name, ".",1) != 0 && strncmp(namelist[i]->d_name, "..",2) != 0)
        {
            // Construct new path from our base path
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, namelist[i]->d_name);

            if(isDir(path)){
                // cek apakah sudah modular
                if(strncmp(namelist[i]->d_name,"module_",7)==0){
                    char mod_path[1000],folder_name[100];
                    strcpy(folder_name,namelist[i]->d_name);
                    sprintf(mod_path,"%s/%s",basePath,&folder_name[7]);
                    rename(path,mod_path);
                    unmodulardir(mod_path);
                }
                else{
                    unmodulardir(path);
                }
            }

            else{
                char newPath[1000];
                struct stat *buf=(struct stat*)malloc(sizeof(struct stat));
                stat(path,buf);
                strncpy(newPath,path,strlen(path)-4);
                newPath[strlen(path)-4]='\0';
                if(strcmp(prevPath,newPath)!=0){
                    strcpy(prevPath,newPath);
                    unmodularize(newPath,buf->st_mode);
                }

                free(buf);
            }
        }
    }

    closedir(dir);
}

// get folder or file attribute
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char dir[300];
    char fullPath[1000];
    int num=0;
    struct stat *st=(struct stat*)malloc(sizeof(struct stat));
    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);
    sprintf(fullPath,"%s.000",path);

    if(strncmp(dir,"module_",7)==0 && access(fullPath,F_OK)==0){
        res=lstat(fullPath,stbuf);
        if(res==-1) return -errno;
        stbuf->st_size=0;

        while(access(fullPath,F_OK)==0){
            res=lstat(fullPath,st);
            if(res==-1) return -errno;

            stbuf->st_size+=st->st_size;
            num++;
            sprintf(fullPath,"%s.00%d",path,num);
        }
    }
    else{
        res = lstat(path,stbuf);
        if(res==-1) return -errno;
    }

    free(st);
    
    fuselog("REPORT","STAT",path);

    return 0;
}

// check file access permission
static int xmp_access(const char *path, int mode){
    int res;
    char dir[300];
    char fullPath[1000];
    int num=0;
    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    if(strncmp(dir,"module_",7)==0 && access(path,F_OK)==0){
        sprintf(fullPath,"%s.00%d",path,num);
        while(access(fullPath,F_OK)==0){
            res=access(fullPath,mode);
            if(res==-1) return -errno;

            num++;
            sprintf(fullPath,"%s.00%d",path,num);
        }
    }
    else{
        res = access(path,mode);
        if(res==-1) return -errno;
    }

    fuselog("REPORT","ACCESS",path);

    return 0;
}

// read the target of symbolyc link
static int xmp_readlink(const char *restrict path, char *restrict buf, size_t bufsize){
    int res = readlink(path,buf,bufsize);

    if(res==-1) return -errno;

    fuselog("REPORT","READLINK",path);

    return 0;
}

// read a directory
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent **namelist;
    char *dirName;
    bool modular=false;
    char fileName[256];
    char prevFile[256];
    char currPath[1000];
    int numfile=0;
    (void) offset;
    (void) fi;

    strcpy(prevFile,"");
    dirName=strrchr(path, '/')+1;
    if(strncmp(dirName,"module_",7)==0) modular=true;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    numfile=scandir(path,&namelist,NULL,alphasort);

    if(numfile<0){
        closedir(dp);
        return -ENOENT;
    }

    for(int i=0; i<numfile; i++) {
        strcpy(currPath, path);
        strcat(currPath, "/");
        strcat(currPath, namelist[i]->d_name);
        struct stat *st=(struct stat *) malloc(sizeof(struct stat));
        stat(currPath,st);

        st->st_ino = namelist[i]->d_ino;
        st->st_mode = namelist[i]->d_type << 12;

        if(modular && S_ISREG(st->st_mode) && strncmp(namelist[i]->d_name, ".",1) != 0 && strncmp(namelist[i]->d_name, "..",2) != 0){
            strncpy(fileName,namelist[i]->d_name,strlen(namelist[i]->d_name)-4);
            fileName[strlen(namelist[i]->d_name)-4]='\0';
            if(strcmp(prevFile,fileName)!=0){
                if(filler(buf, fileName, st, 0)) break;
                strcpy(prevFile,fileName);
            }
        }
        else
            if(filler(buf, namelist[i]->d_name, st, 0)) break;

        free(st);
    }
    closedir(dp);

    fuselog("REPORT","READDIR",path);

    return 0;
}

// create a file node
static int xmp_mknod(const char *path, mode_t mode, dev_t dev){
    int res = mknod(path,mode,dev);

    if(res==-1) return -errno;

    fuselog("REPORT","MKNOD",path);

    return 0;
}

// make new directory
static int xmp_mkdir(const char *folder, mode_t md){
    int res;
    char dir[300];
    strcpy(dir, folder);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    if(strlen(dir)<7)
        res = mkdir(folder, md);

    else if(strncmp(dir,"module_",7)!=0)
        res = mkdir(folder, md);

    else{
        char modPath[400], modFolder[50], module[10];
        strcpy(module,"/module_");
        strcpy(dir, folder);
        strcpy(dir,dirname(dir));

        strcpy(modFolder, folder);
        strcpy(modFolder,strrchr(modFolder,'/')+1);
        sprintf(modPath,"%s%s%s",dir,module,modFolder);

        res = mkdir(modPath, md);
    }

    if (res==-1) return -errno;

    fuselog("REPORT","MKDIR",folder);

    return res;
}

// create a symbolic link
static int xmp_symlink(const char* path1, const char* path2){
    int res = symlink(path1,path2);

    if(res==-1) return -errno;

    char desc[100];
    sprintf(desc,"%s::%s",path1,path2);
    fuselog("REPORT","SYMLINK",desc);

    return 0;
}

// remove a file
static int xmp_unlink(const char* path){
    int res = unlink(path);

    if(res==-1) return -errno;

    fuselog("FLAG","UNLINK",path);

    return 0;
}

// remove directory
static int xmp_rmdir(const char* path){
    int res = rmdir(path);

    if(res==-1) return -errno;

    fuselog("FLAG","RMDIR",path);

    return 0;
}

// rename file
static int xmp_rename(const char* path1, const char* path2){
    int res = rename(path1,path2);
    char dir1[100], dir2[100];
    strcpy(dir1, path1);
    strcpy(dir1, strrchr(dir1,'/')+1);
    strcpy(dir2, path2);
    strcpy(dir2, strrchr(dir2,'/')+1);

    if(res==-1) return -errno;

    if(strncmp(dir1,"module_",7)!=0 && strncmp(dir2,"module_",7)==0){
        char path[1000];
        strcpy(path,path2);
        modulardir(path);
    }

    else if(strncmp(dir1,"module_",7)==0 && strncmp(dir2,"module_",7)!=0){
        char path[1000];
        strcpy(path,path2);
        unmodulardir(path);
    }

    char desc[100];
    sprintf(desc,"%s::%s",path1,path2);
    fuselog("REPORT","RENAME",desc);

    return 0;
}

// create hard link to a file
static int xmp_link(const char* path1, const char* path2){
    int res = link(path1, path2);

    if(res==-1) return -errno;

    char desc[100];
    sprintf(desc,"%s::%s",path1,path2);
    fuselog("REPORT","LINK",desc);

    return 0;
}

// change file mode
static int xmp_chmod(const char*path, mode_t mode){
    int res = chmod(path,mode);

    if(res==-1) return -errno;

    char desc[100];
    sprintf(desc,"%s::%04o",path,mode);
    fuselog("REPORT","CHMOD",desc);

    return 0;
}

// change file owner
static int xmp_chown(const char* path, uid_t owner, gid_t group){
    int res = chown(path,owner,group);

    if(res==-1) return -errno;

    char desc[100];
    sprintf(desc,"%s::%d::%d",path,owner,group);
    fuselog("REPORT","CHOWN",desc);

    return 0;
}

// change size of file
static int xmp_truncate(const char* path, off_t offset){
    int res=truncate(path,offset);

    if(res==-1) return -errno;

    char desc[100];
    sprintf(desc,"%s::%ld",path,offset);
    fuselog("REPORT","TRUNCATE",desc);

    return 0;
}

// change access and modification times of a file with nanosecond resolution
static int xmp_utimens(const char *path, const struct timespec tv[2]){    
    int res = utimensat(AT_FDCWD,path,tv,0);

    if(res==-1) return -errno;

    fuselog("REPORT","UTIMENS",path);

    return 0;
}

// open a file
static int xmp_open(const char* path, struct fuse_file_info *fi){
    int res;
    char dir[300];
    char mod_path[300];
    int cnt=0;

    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    // modular
    if(strncmp(dir,"module_",7)==0){
        strcpy(mod_path,path);
        strcat(mod_path,".000");

        while(access(mod_path,F_OK)==0){
            res=open(mod_path,O_RDONLY);
            if(res==-1) return -errno;

            cnt++;
            sprintf(mod_path,"%s.00%d",path,cnt);
        }
        fi->fh=res;
    }

    else{
        res=open(path,fi->flags);
        if (res==-1) return -errno;
        fi->fh=res;
    }

    fuselog("REPORT","OPEN",path);

    return 0;
}

// read a file
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res=0;
    (void) fi;
    char dir[300];
    char mod_path[300];
    char *temp_buf=(char *)malloc(sizeof(char)*MAXSIZE);
    int cnt=0;
    int numBytes=0;

    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    // modular
    if(strncmp(dir,"module_",7)==0){
        strcpy(mod_path,path);
        strcat(mod_path,".000");

        strcpy(buf,"");
        while(access(mod_path,F_OK)==0){
            fd=open(mod_path,O_RDONLY);
            if(fd==-1) return -errno;

            numBytes=pread(fd, temp_buf, MAXSIZE,offset);
            strncat(buf,temp_buf,numBytes);
            res+=numBytes;
            cnt++;
            sprintf(mod_path,"%s.00%d",path,cnt);

            close(fd);
        }
    }

    // tidak modular
    else{
        fd = open(path, O_RDONLY);

        if(fd==-1) return -errno;

        res = pread(fd, buf, size, offset);

        if (res == -1) res = -errno;
        close(fd);
    }

    fuselog("REPORT","READ",path);

    return res;
}

// write a file
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    int res;
    int fd;
    char dir[300];
    struct stat *st=(struct stat*)malloc(sizeof(struct stat));
    mode_t md;

    if(fi==NULL) {
        mode_t mode=S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH;
        fd=open(path,O_WRONLY | O_CREAT,mode);
    }

    else{
        fd=open(path,O_WRONLY);
    }
    
    if(fd==-1) return -errno;

    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    if(strlen(dir)<7) 
        res=pwrite(fd,buf,size,offset); // tidak perlu dimodularisasi

    else if(strncmp(dir,"module_",7)!=0)
        res=pwrite(fd,buf,size,offset); // tidak perlu dimodularisasi

    else{
        res=stat(path,st);

        if(res==-1) return -1;

        md=st->st_mode;

        res=pwrite(fd,buf,size,offset);
        if(res==-1) return -1;
        
        res=modularize(path,size,md);
    }

    if(res==-1) return -errno;

    free(st);

    fuselog("REPORT","WRITE",path);

    return 0;
}

// get file system statistics
static int xmp_statfs(const char *path, struct statvfs *buf){
    int res=statvfs(path,buf);

    if(res==-1) return -errno;

    fuselog("REPORT","STATVFS",path);

    return 0;
}

// create and open a file
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi){
    int res;
    // char *dir;

    if(fi) res = open(path,fi->flags);

    else res = creat(path,mode);

    if(chmod(path,mode)==-1) return -errno;

    if(res==-1) return -errno;

    if(fi) fi->fh=res;

    fuselog("REPORT","CREATE",path);

    return 0;
}

// release an open file
static int xmp_release(const char *path, struct fuse_file_info *fi){
    int res=close(fi->fh);
    char dir[300];
    char mod_path[300];
    int cnt=0;

    strcpy(dir, path);
    strcpy(dir, dirname(dir));
    strcpy(dir, strrchr(dir,'/')+1);

    // modular
    if(strncmp(dir,"module_",7)==0){
        strcpy(mod_path,path);
        strcat(mod_path,".000");

        while(access(mod_path,F_OK)==0){
            res=open(mod_path,O_RDONLY);
            if(res==-1) return -errno;

            cnt++;
            sprintf(mod_path,"%s.00%d",path,cnt);
        }
    }

    else{
        res=open(path,fi->flags);
        if (res==-1) return -errno;
    }
    (void) path;

    if(res==-1) return -errno;

    fuselog("REPORT","CLOSE",path);

    return 0;
}

// synchronize file contents
static int xmp_fsync(const char *path, int x, struct fuse_file_info *fi){
    (void) x;

    if(fi){
        if(fi->fh!=0){
            int res=fsync(fi->fh);

            if(res==-1) return -errno;
        }
    }

    fuselog("REPORT","FSYNC",path);

    return 0;
}

// set extended attributes
static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags){
    int res=setxattr(path,name,value,size,flags);

    if(res==-1) return -errno;

    fuselog("REPORT","SETXATTR",path);

    return 0;
}

// get extended attributes
static int xmp_getxattr(const char *path, const char *name, char *value, size_t size){
    int res = getxattr(path,name,value,size);

    if(res==-1) return -errno;

    fuselog("REPORT","GETXATTR",path);

    return 0;
}

// list extended attributes
static int xmp_listxattr(const char *path, char *list, size_t size){
    int res=listxattr(path,list,size);

    if(res==-1) return -errno;

    fuselog("REPORT","LISTXATTR",path);

    return 0;
}

// remove extended attributes
static int xmp_removexattr(const char *path, const char *name){
    int res=removexattr(path,name);

    if(res==-1) return -errno;

    fuselog("REPORT","REMOVEXATTR",path);

    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .symlink = xmp_symlink,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .link = xmp_link,
    .chmod = xmp_chmod,
    .chown = xmp_chown,
    .truncate = xmp_truncate,
    .utimens = xmp_utimens,
    .open = xmp_open,
    .read = xmp_read,
    .write = xmp_write,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,
};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}